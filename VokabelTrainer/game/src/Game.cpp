#include "Game.h"
#include <stdexcept>

/// This class handles all the necessary parts of the game.
/// It handles the window in which the game is displayed, all graphical elements and the interactions with the window
/// and all displayed elements.
namespace fs = std::filesystem;
namespace gl3{
    void Game::framebuffer_size_callback(GLFWwindow *window, int width, int height) {
        glViewport(0, 0, width, height);
    }

    Game::Game(int width, int height, const std::string &title) {
        if(!glfwInit()){
            throw std::runtime_error("Failed to initialize glfw");
        }

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

        window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
        if(window == nullptr) {
            throw std::runtime_error("Failed to create window");
        }
        glfwMakeContextCurrent(window);
        glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
        gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

        if (glGetError() != GL_NO_ERROR) {
            throw std::runtime_error("gl error");
        }
    }

    glm::mat4 Game::calculateMvpMatrix(glm::vec3 position, float zRotationInDegrees, glm::vec3 scale){
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, position);
        model = glm::scale(model, scale);
        model = glm::rotate(model, glm::radians(zRotationInDegrees), glm::vec3(0.0f, 0.0f, 1.0f));

        glm::mat4 view = glm::lookAt(glm::vec3(0.0, 0.0, 90.0f),
                                     glm::vec3(0.0f, 0.0f, 0.0),
                                     glm::vec3(0.0, 1.0, 0.0));

        glm::mat4 projection = glm::perspective(glm::radians(2.0f), 800.0f/950.0f, 0.1f, 100.0f);
        return projection * view * model;
    }

    float roundStep(float x, float step)
    {
        return round(x * step) / step;
    }

    bool AreSame(double a, double b)
    {
        return fabs(a - b) < 0.000001;
    }

    void Game::colorUpdate(int windowHeight)
    {
        static int oldState = GLFW_RELEASE;
        int newState = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        if (newState == GLFW_RELEASE && oldState == GLFW_PRESS){
            double xPos, yPos;
            glfwGetCursorPos(window, &xPos, &yPos);
            glReadPixels(xPos, windowHeight - yPos, 1, 1, GL_RGBA, GL_FLOAT, &pixel);
            glReadPixels(xPos, windowHeight - yPos, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
            depth = roundStep(depth, 1000000);
            if (AreSame(depth, 0.999876) && pixel.w != 0)
            {
                std::cout << "guitar" << '\n';
            }
            std::cout << pixel.w << std::endl;
            std::cout << depth << '\n';
        }
        oldState = newState;
        //spaceShip->update(this, deltaTime);
    }

    void Game::update() {
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS){
            glfwSetWindowShouldClose(window, true);
        }
    }

    void Game::draw() {
        glClearColor(0.2f, 0.3f, 0.3f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        const char* fileNameButReally;
        for(int i = 0; i < namesOfImages.size(); i++) {
            auto l_front = namesOfImages.begin();
            std::advance(l_front, i);
            auto fileName = ("../../assets/images/" + *l_front);
            fileNameButReally = fileName.c_str();
            Image image(fileNameButReally, glm::vec3(0.0f + (i/2), 0.0f + (i/2), 0.0f + i), 0.0f, glm::vec3(1.0f, 1.0f, 1.0f));
            image.draw(this);
        }
        /*Image image("../../assets/images/crown.png", glm::vec3(1.0f, 1.0f, 0.0f), 0.0f, glm::vec3(1.0f, 1.0f, 1.0f));
        image.draw(this);
        Image secondImage("../../assets/images/guitar.png", glm::vec3(0.0f, 0.0f, 0.1f), 0.0f, glm::vec3(1.0f, 1.0f, 1.0f));
        secondImage.draw(this);*/
        glfwSwapBuffers(window);
    }

    void Game::run(){
        unsigned int VAO;
        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);

        int height, width;
        glfwGetWindowSize(window, &width, &height);
        glfwSetTime(1.0 / 60);

        std::string path = "../../assets/images";
        float i = 0;
        for (const auto & entry : fs::directory_iterator(path)) {
            std::string fileName = std::filesystem::path(entry).stem().string() + std::filesystem::path(entry).extension().string();
            namesOfImages.push_back(fileName);
            depthOfImages.push_back(i);
            i += 1.0f;
        }

        /*for(int i = 0; i < namesOfImages.size(); i++){
            auto l_front = namesOfImages.begin();
            std::advance(l_front, i);
            auto fileName = ("../../assets/images/" + *l_front);
            const char* fileNameButReally = fileName.c_str();
            std::cout << fileNameButReally << '\n';
            Image image(fileNameButReally, glm::vec3(1.0f, 1.0f, 0.0f), 0.0f, glm::vec3(1.0f, 1.0f, 1.0f));
            image.draw(this);
        }*/

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        glBindVertexArray(VAO);

        while(!glfwWindowShouldClose(window)) {
            draw();
            update();
            colorUpdate(height);
            updateDeltaTime();
            glfwPollEvents();
        }
        glDeleteVertexArrays(1, &VAO);

        return;
    }

    void Game::updateDeltaTime() {
        float frameTime = glfwGetTime();
        deltaTime = frameTime - lastFrameTime;
        lastFrameTime = frameTime;
    }

    Game::~Game() {
        glfwTerminate();
    }

    GLFWwindow *Game::getWindow() const {
        return window;
    }
}