#include "Game.h"

int main() {
    try {
        gl3::Game vocabGame(800, 950, "SearchToLearn");
        vocabGame.run();
    }
    catch(const std::exception &e) {
        std::cerr << "Unhandled exception" << e.what() << std::endl;
    }
}