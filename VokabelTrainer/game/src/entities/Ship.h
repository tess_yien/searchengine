#pragma once

#include <iostream>
#include "Entity.h"

namespace gl3{
    class Game;
    class Ship: public Entity {
    public:
        explicit Ship(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
                      float zRotation = 0.0f,
                      glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f));
        //virtual void update(Game *game, float deltaTime) override;
        glm::vec3 shipPosition = glm::vec3(0,0,0);
    private:
        //float translationSpeed = 1.0f;
        //float rotationSpeed = 120.0f;
        //float velocity = 1.0f;
        //float acceleration = 2.0f;
    };
}



