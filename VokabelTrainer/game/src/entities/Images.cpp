#include "Images.h"
#include "../Game.h"

/// This class sets the base elements for the images to load into the game.
/// It induces the basic components the image needs and provides the
/// opportunity to compute the image according to those graphically
/// influencing components such as ths position, rotation around the z-Axis,
/// scale and Mesh.

namespace gl3{

    Images::Images(Shader shader, Mesh mesh, glm::vec3 position, float zRotation, glm::vec3 scale) :
            shader(std::move(shader)),
            mesh(std::move(mesh)),
            position(position),
            zRotation(zRotation),
            scale(scale)
    {
    }

    void Images::draw(Game *game)
    {
        auto mvpMatrix = game->calculateMvpMatrix(position, zRotation, scale);
        shader.use();
        shader.setMatrix("mvp", mvpMatrix);
        mesh.draw();
    }
}