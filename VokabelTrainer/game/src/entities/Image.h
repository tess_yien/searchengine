#pragma once

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <filesystem>
#include "Images.h"

namespace fs = std::filesystem;

namespace gl3
{
    class Image : public Images {
    public:
        explicit Image(const char *imagePath,
                       glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
                       float zRotation = 0.0f,
                       glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f));
        void loadImage(const char *imagePath);
        void customizeTexture();
        void generateTexture();
    private:
        unsigned int texture;
    };
}


