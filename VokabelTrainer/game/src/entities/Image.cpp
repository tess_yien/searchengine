#include "Image.h"
#define STB_IMAGE_IMPLEMENTATION
#include <../../extern/stb_image.h>

/// This class provides the functions to generate, load and edit a 2D texture to prepare it for display in the window rendering.
/// It provides the image with customizable options for the position, rotation, scale, shape (Mesh) in the physical
/// representation and sets texture specific options for correct display.
namespace gl3
{
    Image::Image(const char *imagePath, glm::vec3 position, float zRotation, glm::vec3 scale) : Images(
            Shader("shaders/imageVertexShader.vert", "shaders/imageFragmentShader.frag"),
            Mesh({
                         // positions          // colors           // texture coords
                         0.5f,  0.5f, 0.0f,    1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
                         0.5f, -0.5f, 0.0f,    0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
                         -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
                         -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left
                 },
                 {
                         0, 1, 3, // first triangle
                         1, 2, 3  // second triangle
                 }),
            position,
            zRotation,
            scale)
    {
        generateTexture();
        customizeTexture();
        loadImage(imagePath);
    }

    void Image::loadImage(const char *imagePath)
    {
        int imageWidth, imageHeight, numChannels;
        stbi_set_flip_vertically_on_load(true);
        unsigned char* imageData = stbi_load(imagePath, &imageWidth, &imageHeight, &numChannels, 0);
        if (imageData) // handle exception for non existing image or inaccurate image path
        {
            // Set image data
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
        }
        else
        {
            std::cerr << "Failed to load image" << std::endl;
            glfwTerminate();
        }
        stbi_image_free(imageData);
    }

    // sets texture parameter to repetition of the image rather than stretching it and to
    // linear color interpolation of the pixel color values rather than picking the closest
    // pixel color value
    void Image::customizeTexture()
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    void Image::generateTexture()
    {
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
    }
}