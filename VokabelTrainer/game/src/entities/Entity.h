#pragma once

#include <iostream>
#include "glm/glm.hpp"
#include "../rendering/Shader.h"
#include "../rendering/Mesh.h"
#include "GLFW/glfw3.h"

namespace gl3{
    class Game;

    class Entity {
    public:
        Entity(Shader shader,
               Mesh mesh,
               glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
               float zRotation = 0.0f,
               glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f),
               glm::vec4 color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
        virtual ~Entity() = default;
        virtual void update(Game *game, float deltaTime){};
        virtual void draw(Game *game);

        void setPosition(const glm::vec3 &position);
        void setScale(const glm::vec3 &scale);
        void setZRotation(float zRotation);

        const glm::vec3 &getPosition() const;
        const glm::vec3 &getScale() const;
        float getZRotation() const;

        float zRotation;
    protected:
        glm::vec4 color;
        glm::vec3 scale;
        glm::vec3 position;

        Shader shader;
        Mesh mesh;
    };
}

