#pragma once

#include "Entity.h"

namespace gl3{
    class TextureTest : public Entity{
    public:
        explicit TextureTest(glm::vec3 = glm::vec3(0.0f, 0.0f, 0.0f));
        void draw(Game *game) override;
        void createTexture();
        void loadImage();
    };
}

