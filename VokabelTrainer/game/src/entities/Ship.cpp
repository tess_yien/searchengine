#include "Ship.h"
#include "../Game.h"

namespace gl3{
    Ship::Ship(glm::vec3 position, float zRotation, glm::vec3 scale) : Entity(
                    Shader("shaders/vertexShader.vert", "shaders/fragmentShader.frag"),
                          Mesh({
                                       -0.5f, -0.5f, 0.0f,
                                       0.5f, -0.5f, 0.0f,
                                       0.0f,  0.5f, 0.0f
                          },
                               {0, 1, 2}),
                          position,
                          zRotation,
                          scale,
                          glm::vec4(0.5f, 0.25f, 0.25f, 1.0f)){}

    /*void Ship::update(Game *game, float deltaTime) {
        if (glfwGetKey(game->getWindow(), GLFW_KEY_A) == GLFW_PRESS){
            zRotation += rotationSpeed * deltaTime;
        }
        if (glfwGetKey(game->getWindow(), GLFW_KEY_D) == GLFW_PRESS){
            zRotation -= rotationSpeed * deltaTime;
        }
        if(glfwGetKey(game->getWindow(), GLFW_KEY_W) == GLFW_PRESS){
            position.y += sin(glm::radians(zRotation)) * translationSpeed * deltaTime;
            position.x += cos(glm::radians(zRotation)) * translationSpeed * deltaTime;
        }
        if(glfwGetKey(game->getWindow(), GLFW_KEY_S) == GLFW_PRESS){
            position.y -= sin(glm::radians(zRotation)) * translationSpeed * deltaTime;
            position.x -= cos(glm::radians(zRotation)) * translationSpeed * deltaTime;
        }
        if(glfwGetKey(game->getWindow(), GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS){
            translationSpeed = velocity * acceleration;
        }
        else{
            translationSpeed = velocity;
        }
    }*/
}