#include "TextureTest.h"
#include "../Game.h"
#include "../../extern/stb_image.h"

namespace gl3
{
    TextureTest::TextureTest(glm::vec3 position): Entity(
            Shader("shaders/imageVertexShader.vert", "shaders/imageFragmentShader.frag"),
            Mesh (
            {
                    // positions          // colors           // texture coords
                    0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
                    0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
                    -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,  0.0f, 0.0f,// bottom left
                    -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,  0.0f, 1.0f// top left
            },
            {
                0, 1, 3,    //first triangle
                1, 2, 3     //second triangle
            }),
            position,
            0.0f,
            glm::vec3(1.0f, 1.0f, 1.0f),
            glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
            ) {}

    void TextureTest::loadImage()
    {
        int width, height, nrChannels;
        unsigned char *data = stbi_load("../../assets/images/guitar.png", &width, &height, &nrChannels, 0);
        if(data)
        {
            //TO DO: set GL_RGB/GL_RGBA according to file format (jpg/png)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        }
    }

    void TextureTest::createTexture()
    {
        unsigned int texture = 0;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        TextureTest::loadImage();
    }

    void TextureTest::draw(Game *game)
    {
        auto mvpMatrix = game->calculateMvpMatrix(position, zRotation, scale);
        shader.use();
        shader.setMatrix("mvp", mvpMatrix);
        shader.setVector("color", color);
        mesh.draw();
        mesh.handleVertexAttributes(1, 3, 8, 3);
        mesh.handleVertexAttributes(2, 2, 8, 6);
    }
}