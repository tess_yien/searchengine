#pragma once

#include <glm/glm.hpp>
#include "../rendering/Shader.h"
#include "../rendering/Mesh.h"

namespace gl3
{
    class Game;

    class Images {
    public:
        Images(Shader shader,
               Mesh mesh,
               glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
               float zRotation = 0,
               glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f));
        void draw(Game *game);
    protected:
        glm::vec3 position;
        float zRotation;
        glm::vec3 scale;
        Shader shader;
        Mesh mesh;
    };
}


