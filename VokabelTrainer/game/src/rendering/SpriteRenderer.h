#pragma once

#include "Shader.h"
#include "Texture.h"
#include "../entities/Entity.h"

namespace gl3{
    class SpriteRenderer : public Entity {
    public:
        SpriteRenderer(glm::vec3 position, float zRotation, glm::vec3 scale);
        //~SpriteRenderer();
    private:
        //Shader shader;
        unsigned int quadVAO;
    };
}


