#pragma once
#include <iostream>
#include <filesystem>
#include "glad/glad.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

namespace fs = std::filesystem;

namespace gl3{
    class Shader {
    public:
        Shader(const fs::path &vertexShaderPath, const fs::path &fragmentShaderPath);
        Shader(const Shader &shader) = delete;
        Shader(Shader &&other) noexcept{
            std::swap(this->shaderProgram, other.shaderProgram);
            std::swap(this->vertexShader, other.vertexShader);
            std::swap(this->fragmentShader, other.fragmentShader);
        };
        ~Shader();

        void setMatrix(const std::string &uniformName, glm::mat4 matrix);
        void setVector(const std::string &uniformName, glm::vec4 vector);

        void use();
    private:
        unsigned int shaderProgram = 0;
        unsigned int vertexShader = 0;
        unsigned int fragmentShader = 0;

        unsigned int loadAndCompileShader(GLuint shaderType, const fs::path &shaderPath);
    };
}

