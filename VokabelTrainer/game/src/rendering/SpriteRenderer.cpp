#include "SpriteRenderer.h"

namespace gl3{

    SpriteRenderer::SpriteRenderer(glm::vec3 position, float zRotation, glm::vec3 scale): Entity (
            Shader("shaders/imageVertexShader.vert", "shaders/imageFragmentShader.frag"),
            Mesh({
                     //vertices           //texCoords
                     0.0f, 1.0f,          0.0f, 1.0f,
                     1.0f, 0.0f,          1.0f, 0.0f,
                     0.0f, 0.0f,          0.0f, 0.0f,

                     0.0f, 1.0f,          0.0f, 1.0f,
                     1.0f, 1.0f,          1.0f, 1.0f,
                     1.0f, 0.0f,          1.0f, 0.0f},
                    {
                        0, 1, 2,
                        0, 2, 3
                    }),
            position,
            zRotation,
            scale){
    }
}