#pragma once

#include <iostream>
#include <memory>
#include <random>
#include "glad/glad.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "GLFW/glfw3.h"
#include "rendering/Shader.h"
#include "rendering/Mesh.h"
#include "rendering/SpriteRenderer.h"
#include "entities/Ship.h"
#include "entities/TextureTest.h"
#include "entities/Image.h"
#include <string>
#include <filesystem>
#include <list>

namespace gl3{
    class Game {
    public:
        void run();
        Game(int width, int height, const std::string &title);
        glm::mat4 calculateMvpMatrix(glm::vec3 position, float zRotationInDegrees, glm::vec3 scale);
        float getZRotation() const;

        GLFWwindow *getWindow() const;
        void setWindow(GLFWwindow *window);

        virtual ~Game();
    private:
        glm::vec4 pixel;
        glm::vec3 position;
        float depth;
        bool clicked = false;
        void colorUpdate(int windowHeight);
        void update();
        void draw();
        void updateDeltaTime();
        void loadImage();
        static void framebuffer_size_callback(GLFWwindow *window, int width, int height);
        GLFWwindow *window = nullptr;
        glm::mat4 mvpMatrix;
        float lastFrameTime = 1.0f/60;
        float deltaTime = 1.0f/60;
        std::unique_ptr<Ship> spaceShip;
        std::unique_ptr<Ship> spaceShipNumberTwo;
        std::unique_ptr<TextureTest> textureTest;
        Ship* ship;
        unsigned int texture;
        std::unique_ptr<SpriteRenderer> renderer;
        std::list<std::string> namesOfImages;
        std::list<float> depthOfImages;
    };
}