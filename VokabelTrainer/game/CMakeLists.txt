cmake_minimum_required(VERSION 3.24)

set(SOURCE_FILES src/main.cpp src/Game.cpp src/Game.h src/rendering/Shader.cpp src/rendering/Shader.h src/rendering/Mesh.cpp src/rendering/Mesh.h src/entities/Entity.cpp src/entities/Entity.h src/Assets.h src/rendering/SpriteRenderer.cpp src/rendering/SpriteRenderer.h src/entities/Ship.cpp src/entities/Ship.h src/rendering/Texture.cpp src/rendering/Texture.h src/entities/TextureTest.cpp src/entities/TextureTest.h src/entities/Image.cpp src/entities/Image.h src/entities/Images.cpp src/entities/Images.h)

set(EXE_FILE game_lab_3)

add_executable(${EXE_FILE} ${SOURCE_FILES})

target_compile_features(${EXE_FILE} PRIVATE cxx_std_17)
target_compile_definitions(${EXE_FILE} PRIVATE DEBUG_ASSET_ROOT=${PROJECT_SOURCE_DIR}/assets)

#link dependencies
target_link_libraries(${EXE_FILE} PRIVATE glad glfw glm)