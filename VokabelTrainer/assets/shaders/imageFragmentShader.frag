#version 450

out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D imageTexture;

void main() {
    if(texture(imageTexture, TexCoord).a < 0.1)
    {
        discard;
    }
    FragColor = texture(imageTexture, TexCoord);
}
